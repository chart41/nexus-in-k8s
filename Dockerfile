FROM centos:7.9.2009
LABEL maintainer="Yaroslav"
LABEL version="3"
LABEL description="Docker Image with Nexus"
ENV TZ=Europe/Moscow

RUN yum update -y \
    && yum -y install java-1.8.0-openjdk java-1.8.0-openjdk-devel \
    && yum install wget -y \
    && wget https://sonatype-download.global.ssl.fastly.net/repository/downloads-prod-group/3/nexus-3.41.0-01-unix.tar.gz \
    && tar -C "/usr/local/" -xvzf nexus-3.41.0-01-unix.tar.gz \
    && chmod 755 -R /usr/local/nexus-3.41.0-01 /usr/local/sonatype-work \
    && rm nexus-3.41.0-01-unix.tar.gz \
    && yum remove wget -y \
    && yum clean all
CMD ["/usr/local/nexus-3.41.0-01/bin/nexus", "run"]
EXPOSE 8081
